﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerLite.SL.Scheduler
{
    /// <summary>
    /// https://codinginfinite.com/creating-scheduler-task-seconds-minutes-hours-days/
    /// </summary>
    public class SchedulerRegistry
    {
        public static void IntervalInSeconds(int hour, int sec, double interval, Action task)
        {    
            interval = interval / 3600;
            SchedulerManager.Instance.ScheduleTask(hour, sec, interval, task);
        }
        public static void IntervalInMinutes(int hour, int min, double interval, Action task)
        {
            interval = interval / 60;
            SchedulerManager.Instance.ScheduleTask(hour, min, interval, task);
        }
        public static void IntervalInHours(int hour, int min, double interval, Action task)
        {
            SchedulerManager.Instance.ScheduleTask(hour, min, interval, task);
        }
        public static void IntervalInDays(int hour, int min, double interval, Action task)
        {
            interval = interval * 24;
            SchedulerManager.Instance.ScheduleTask(hour, min, interval, task);
        }
    }
}
