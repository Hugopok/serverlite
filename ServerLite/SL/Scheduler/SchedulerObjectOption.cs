﻿namespace ServerLite.SL.Scheduler
{
    [System.Serializable]
    public class SchedulerObjectOption
    {
        public int Days = -1;
        public int Hour = -1;
        public int Minutes = -1;
        public int Seconds = -1;

        public double interval = 2.0;

        public ScheduleIntervalPer intervalType;
    }

    public enum ScheduleIntervalPer
    {
        Seconds,
        Minutes,
        Hours,
        Days
    }
}
