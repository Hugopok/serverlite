﻿namespace ServerLite.SL.Models
{
    public class JsonPackage<T>
    {
        public string requestType;
        public string error;
        public T content;
    }
}
