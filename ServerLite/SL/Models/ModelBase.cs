﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace ServerLite.SL.Models
{
    [Serializable]
    public class ModelBase
    {
        [BsonId]
        public Guid id;
    }
}
