﻿using System;
using System.Net.Sockets;

namespace ServerLite.SL.Models
{
    /// <summary>
    /// Class to manage client
    /// </summary>
    public class MClient
    {
        public Guid id;

        public string endPoint;

        public TcpClient socket;

        public bool isLogged = false;

    }
}
