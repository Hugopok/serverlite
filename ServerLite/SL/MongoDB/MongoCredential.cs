﻿namespace ServerLite.SL.MongoDB
{
    public class MongoCredential
    {
        public string UriComposed
        {
            get
            {
                if (string.IsNullOrEmpty(ipHost))
                    return string.Empty;
                return $"mongodb://{username}:{password}@{ipHost}:{port}/?authSource=admin";
            }
        }
        
        public string username;
        public string password;
        public string ipHost;
        public int port;
    }
}
