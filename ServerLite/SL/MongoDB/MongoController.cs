﻿using System.Collections.Generic;
using MongoDB.Driver;
using System;
using System.Linq;
using ServerLite.SL.LogSystem;

namespace ServerLite.SL.MongoDB
{
    // Document = Json class
    // Collection = List di document

    public static class MongoController
    {
        private static IMongoDatabase _currentDB;

        public static IMongoDatabase GetOrCreateDB(string dbName,string uri = "")
        {
            MongoClient client;
            if (string.IsNullOrEmpty(uri))
                client = new MongoClient();
            else
                client = new MongoClient(uri);

            _currentDB = client.GetDatabase(dbName);
            
            if (_currentDB != null)
                Console.WriteLine("DB created!");
            else
                Console.WriteLine("Hmm something is gone wrong");

            return _currentDB;
        }

        public static IMongoCollection<T> GetCollection<T>(string collectionName)
        {
            try
            {
                return _currentDB.GetCollection<T>(collectionName);
            }
            catch (Exception e)
            {
                Log.AddLog($"Error during getting collection with name: {collectionName} ||  MESSAGE ERROR: {e.Message}", true);
            }

            return null;
        }

        public static void InsertDocument<T>(string collectionName, T document)
        {

            var list = _currentDB.GetCollection<T>(collectionName);

            if (list == null)
            {
                _currentDB.CreateCollection(collectionName);
                list = _currentDB.GetCollection<T>(collectionName);
            }

            try
            {
                list.InsertOne(document);
            }
            catch (Exception e)
            {
                Log.AddLog($"Mongo Controller error during document insert Document typeof {typeof(T).Name} : {e.Message}", true);
            }

        }

        public static ReplaceOneResult UpdateCollection<T>(string collectionName,Guid id,T newDoc)
        {
            var builder = Builders<T>.Filter;

            var collection = _currentDB.GetCollection<T>(collectionName);

            var result = collection.ReplaceOne(builder.Eq("id",id),newDoc);

            return result;
        }

        public static void DeleteDocument<T>(string collectionName, Guid id)
        {
            lock (_currentDB)
            {
                var list = _currentDB.GetCollection<T>(collectionName);

                var deleteFilter = Builders<T>.Filter.Eq("id", id);
                list.DeleteOne(deleteFilter);
            }
        }

        public static T GetDocument<T>(string collectionName, string valueName, string value)
        {
            lock (_currentDB)
            {
                var list = _currentDB.GetCollection<T>(collectionName);

                if (list != null)
                {
                    var filter = Builders<T>.Filter.Eq(valueName, value);

                    var result = list.Find<T>(filter);

                    if (result != null && result.ToList().Count > 0)
                        return result.ToList().First();
                }

                return default(T);
            }
        }

        public static T GetDocument<T>(string collectionName, string valueName, int value)
        {
            lock (_currentDB)
            {
                var list = _currentDB.GetCollection<T>(collectionName);

                if (list != null)
                {
                    var filter = Builders<T>.Filter.Eq(valueName, value);

                    var result = list.Find<T>(filter);

                    if (result != null && result.ToList().Count > 0)
                        return result.ToList().First();
                }

                return default(T);
            }
        }

        public static T GetDocument<T>(string collectionName, string valueName, Guid value)
        {
            lock (_currentDB)
            {
                var list = _currentDB.GetCollection<T>(collectionName);

                if (list != null)
                {
                    var filter = Builders<T>.Filter.Eq(valueName, value);

                    var result = list.Find<T>(filter);

                    if (result != null && result.ToList().Count > 0)
                        return result.ToList().First();
                }

                return default(T);
            }
        }

        public static T GetDocument<T>(string collectionName, FilterDefinition<T> filterDefinition)
        {
            lock (_currentDB)
            {
                var list = _currentDB.GetCollection<T>(collectionName);

                if (list != null)
                {
                    var result = list.Find<T>(filterDefinition);
                    if (result != null && result.ToList().Count > 0)
                        return result.First();
                }

                return default(T);
            }
        }

        public static List<T> GetAll<T>(string collectionName) 
        {
            lock (_currentDB)
            {
                var list = _currentDB.GetCollection<T>(collectionName);

                if (list != null)
                {
                    return list.Find<T>(Builders<T>.Filter.Empty).ToList();
                }
                return null;
            }
        }

        public static List<T> GetDocuments<T>(string collectionName, string valueName, int value)
        {
            lock (_currentDB)
            {
                var list = _currentDB.GetCollection<T>(collectionName);

                if (list != null)
                {
                    var filter = Builders<T>.Filter.Eq(valueName, value);

                    var result = list.Find<T>(filter);

                    if (result != null && result.ToList().Count > 0)
                        return result.ToList();
                }

                return null;
            }
        }

        public static List<T> GetDocuments<T>(string collectionName, FilterDefinition<T> filterDefinition)
        {
            lock (_currentDB)
            {
                var list = _currentDB.GetCollection<T>(collectionName);

                if (list != null)
                {
                    var result = list.Find<T>(filterDefinition);
                    if (result != null)
                        return result.ToList();
                }

                return null;
            }
        }

        public static List<T> GetDocuments<T>(string collectionName, string valueName, string value)
        {
            lock (_currentDB)
            {
                var list = _currentDB.GetCollection<T>(collectionName);

                if (list != null)
                {
                    var filter = Builders<T>.Filter.Eq(valueName, value);

                    var result = list.Find<T>(filter);

                    if (result != null && result.ToList().Count > 0)
                        return result.ToList();
                }

                return null;
            }
        }

        public static List<T> GetDocuments<T>(string collectionName, string valueName, Guid value)
        {
            lock (_currentDB)
            {
                var list = _currentDB.GetCollection<T>(collectionName);

                if (list != null)
                {
                    var filter = Builders<T>.Filter.Eq(valueName, value);

                    var result = list.Find<T>(filter);

                    if (result != null && result.ToList().Count > 0)
                        return result.ToList();
                }

                return null;
            }
        }
    }
}
