﻿using ServerLite.SL.Core;
using ServerLite.SL.Models;

namespace ServerLite.SL.Managers
{
    public interface IController
    {
        ConnectionHandler Handler { get; set; }

        void OnDisconnectedClient(MClient client);
    }
}
