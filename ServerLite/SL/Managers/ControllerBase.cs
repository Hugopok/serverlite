﻿using System;
using ServerLite.SL.Core;
using ServerLite.SL.Models;
using ServerLite.SL.Utils;
using ServerLite.SL.LogSystem;

namespace ServerLite.SL.Managers
{
    public abstract class ControllerBase<T> : IDisposable, IController where T : ModelBase
    {
        protected MClient client;
        protected T model;

        public ConnectionHandler Handler { get; set; }

        public ControllerBase(T model, MClient socket)
        {
            this.model = model;
            this.client = socket;

            InitializeController();
        }

        public abstract void InitializeController();

        public virtual void OnDisconnectedClient(MClient client)
        {
            if (this.client.id != client.id) return;

            Dispose(true);
        }

        #region Deserialize

        /// <summary>
        /// Generic is used to not read a ModelBase object, so if you want to read just an integer you can do it!
        /// </summary>
        /// <typeparam name="Generic"></typeparam>
        /// <param name="message"></param>
        /// <returns></returns>
        protected virtual JsonPackage<Generic> DeserializeJson<Generic>(string message)
        {
            try
            {
                return JsonHelper.DeserializeJson<Generic>(message);
            }
            catch (Exception e)
            {
                Log.AddLog($"Controller base || JSON || {message} || Exception error || {e.Message}", true);
            }

            return null;
        }

        protected virtual JsonPackage<T> DeserializeJson(string message) 
        {
            try
            {
                return JsonHelper.DeserializeJson<T>(message);
            }
            catch (Exception e)
            {
                Log.AddLog($"Controller base || JSON || {message} || Exception error || {e.Message}", true);
            }

            return null;
        }

        // TODO : Read raw byte

        #endregion

        #region Serialize

        protected virtual string SerializeJson(JsonPackage<T> jsonPackage)
        {
            try
            {
                return JsonHelper.SerializeJson<T>(jsonPackage);
            }
            catch (Exception e)
            {
                Log.AddLog($"Controller base || JSON || {jsonPackage.requestType} || Exception error || {e.Message}", true);
            }

            return null;
        }

        /// <summary>
        /// Generic is used to not send a ModelBase object, so if you want to send just an integer you can do it!
        /// </summary>
        /// <typeparam name="Generic"></typeparam>
        /// <param name="message"></param>
        /// <returns></returns>
        protected virtual string SerializeJson<Generic>(JsonPackage<Generic> jsonPackage)
        {
            try
            {
                return JsonHelper.SerializeJson<Generic>(jsonPackage);
            }
            catch (Exception e)
            {
                Log.AddLog($"Serialize Json error {e.Message}", true);
            }

            return null;
        }

        #endregion

        #region SendMessage

        // TODO : Send raw byte

        protected virtual void SendMessage<Generic>(JsonPackage<Generic> package)
        {
            SendMessage(SerializeJson(package));
        }

        protected virtual void SendMessage(JsonPackage<T> package)
        {
            SendMessage(SerializeJson(package));
        }

        protected virtual void SendMessage(string message)
        {
            Handler.Send(message);
        }

        protected virtual void SendMessageAsync(JsonPackage<T> package)
        {
            SendMessageAsync(SerializeJson(package));
        }

        protected virtual void SendMessageAsync(string message)
        {
            Handler.SendAsync(message);
        }

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // Per rilevare chiamate ridondanti

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: eliminare lo stato gestito (oggetti gestiti).
                    client = null;
                    model = null;
                }

                // TODO: liberare risorse non gestite (oggetti non gestiti) ed eseguire sotto l'override di un finalizzatore.
                // TODO: impostare campi di grandi dimensioni su Null.

                disposedValue = true;
            }
        }

        // TODO: eseguire l'override di un finalizzatore solo se Dispose(bool disposing) include il codice per liberare risorse non gestite.
        // ~ControllerBase()
        // {
        //   // Non modificare questo codice. Inserire il codice di pulizia in Dispose(bool disposing) sopra.
        //   Dispose(false);
        // }

        // Questo codice viene aggiunto per implementare in modo corretto il criterio Disposable.
        void IDisposable.Dispose()
        {
            // Non modificare questo codice. Inserire il codice di pulizia in Dispose(bool disposing) sopra.
            Dispose(true);
            // TODO: rimuovere il commento dalla riga seguente se è stato eseguito l'override del finalizzatore.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
