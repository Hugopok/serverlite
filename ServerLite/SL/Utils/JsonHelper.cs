﻿using Newtonsoft.Json;
using ServerLite.SL.Models;
using System;


namespace ServerLite.SL.Utils
{
    public static class JsonHelper
    {
        public static JsonPackage<T> DeserializeJson<T>(string message)
        {
            try
            {
                return JsonConvert.DeserializeObject<JsonPackage<T>>(message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }

        public static string SerializeJson<ContentType>(JsonPackage<ContentType> jsonPackage)
        {
            try
            {
                return JsonConvert.SerializeObject(jsonPackage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }
    }
}
