﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Reflection;
using ServerLite.SL.Utils;
using ServerLite.SL.Models;
using ServerLite.SL.Managers;
using ServerLite.SL.LogSystem;

namespace ServerLite.SL.Core
{
    // TODO : Refactoring this class
    public class ConnectionHandler : IDisposable
    {
        public bool IsConnected
        {
            get
            {
                return client.socket.Client.Connected && client.socket.Client.IsConnected();
            }
        }

        public Action<MClient> OnClientDisconnected;
        public Action<MClient> OnClientConnected;

        protected MClient client;

        protected StringBuilder stringBuilder = new StringBuilder();
        protected byte[] buffer = new byte[ServerOptions.BUFFER_SIZE];
        protected int bytesRecieved = 0;
        protected List<IController> controllersAttached = new List<IController>();
        protected Dictionary<string, Tuple<MethodInfo,IController>> requestMethods = new Dictionary<string, Tuple<MethodInfo, IController>>();

        public ConnectionHandler(MClient client,List<IController> controllers)
        {
            this.client = client;

            foreach (var controller in controllers)
            {
                if (controller == null) continue;

                controller.Handler = this;

                controllersAttached.Add(controller);
            }

            GetAttributes();

            Task.Run(MonitoringSocket);

            client.socket.Client.ReceiveTimeout = 900000;
            client.socket.Client.SendTimeout = 900000;

            Listen();

            OnClientConnected?.Invoke(client);
        }

        protected virtual async Task MonitoringSocket()
        {
            while (IsConnected)
            {
                await Task.Delay(5000);
            }

            Log.AddLog($"{client.endPoint} Disconnected",false);

            foreach (var controller in controllersAttached)
            {
                controller?.OnDisconnectedClient(client);
            }

            OnClientDisconnected?.Invoke(client);

            Dispose(true);
        }

        protected virtual void MessageRead(string message)
        {
            bytesRecieved = 0;
            Array.Clear(buffer, 0, buffer.Length);

            Log.AddLog($"Message arrived From : {client.endPoint} \n Message : {message}",false);

            try
            {
                var json = JsonHelper.DeserializeJson<JsonPackage<object>>(message);

                if (!requestMethods.ContainsKey(json.requestType)) { Log.AddLog("Request type not found"); return; };

                var tuple = requestMethods[json.requestType];

                if (tuple != null)
                    tuple.Item1.Invoke(tuple.Item2, new object[] { message });
            }
            catch (Exception e)
            {
                Log.AddLog($"Error during json parsing: {e.Message}",true);
            }
        }

        protected virtual void Listen()
        {
            try
            {
                if (IsConnected)
                    client.socket.Client.BeginReceive(buffer, 0, buffer.Length, 0, new AsyncCallback(Read), client.socket.Client);
            }
            catch (Exception e)
            {
                Log.AddLog($"Unable to listen client {e.Message}",true);
            }
        }

        protected virtual void Read(IAsyncResult asyncResult)
        {
            try
            {
                if (asyncResult == null) return;

                Socket s = asyncResult.AsyncState as Socket;

                if (s == null)
                {
                    Listen();
                    return;
                }

                Log.AddLog("Bytes avaiable: " + s.Available);

                SocketError socketError;
                bytesRecieved = s.EndReceive(asyncResult, out socketError);
                string message = stringBuilder.ToString();

                if (socketError == SocketError.Success)
                {
                    Log.AddLog("Bytes readed: " + bytesRecieved);

                    if (bytesRecieved != 0)
                    {
                        stringBuilder.Append(Encoding.ASCII.GetString(buffer, 0, bytesRecieved));

                        message = stringBuilder.ToString();
                    }

                    if (bytesRecieved != buffer.Length) // End message
                    {
                        if (string.IsNullOrEmpty(message)) return;

                        Log.AddLog($"message readed: {bytesRecieved}");

                        // All the data has been read from the
                        // client. Display it on the console.  
                        if (message.Length < 200)
                            Log.AddLog($"Read {message.Length} bytes from socket. \n Data : {message}");

                        MessageRead(message);

                        stringBuilder.Clear();
                    }
                }
                else
                    Log.AddLog($"Socket error: {socketError.ToString()}",true);
            }
            catch (SocketException e)
            {
                Log.AddLog($"Socket Error (SocketException) {e.Message}",true);
            }
            catch (Exception e)
            {
                Log.AddLog($"Generic Error (GenericError) {e.Message}",true);
            }
            finally
            {
                Listen();
            }
        }

        public virtual void Send(string data)
        {
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            try
            {
                // Begin sending the data to the remote device.  
                client.socket.Client.Send(byteData);
            }
            catch (SocketException e)
            {
                Log.AddLog($"ERROR CODE: {e.ErrorCode} Message: {e.Message}",true);
            }
            catch (Exception e)
            {
                Log.AddLog($"ERROR MESSAGE: {e.Message}");
            }
        }

        public virtual void SendAsync(string data)
        {
            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            System.Net.HttpListener httpListener = new System.Net.HttpListener();
            
            // Begin sending the data to the remote device.  
            client.socket.Client.BeginSend(byteData, 
                                           0, 
                                           byteData.Length, 
                                           0, 
                                           new AsyncCallback(SendCallback), 
                                           client.socket.Client);
        }

        protected virtual void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = handler.EndSend(ar);

                Console.WriteLine("Sent {0} bytes to client.", bytesSent);
            }
            catch (Exception e)
            {
                Log.AddLog($"Error during send callback : {e.Message}",true);
            }

            Listen();
        }

        public virtual void GetAttributes()
        {
            foreach (var controller in controllersAttached)
            {
                Type type = controller.GetType();

                foreach (var minfos in type.GetMethods())
                {
                    var m = minfos.GetCustomAttribute(typeof(Request), true);

                    if (m != null)
                    {
                        var request = (Request)m;

                        if (request != null)
                            requestMethods.Add(request.RequestType, new Tuple<MethodInfo, IController>(minfos, controller));
                    }
                }
            }
        }

        #region IDisposable Support

        private bool disposedValue = false; // Per rilevare chiamate ridondanti

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: eliminare lo stato gestito (oggetti gestiti).
                }

                controllersAttached.Clear();
                controllersAttached = null;
                OnClientDisconnected = null;
                OnClientConnected = null;
                stringBuilder.Clear();
                Array.Clear(buffer,0,buffer.Length);
                buffer = null;
                client.socket.Close();
                client.socket.GetStream().Close();
                requestMethods.Clear();
                requestMethods = null;
                // TODO: liberare risorse non gestite (oggetti non gestiti) ed eseguire sotto l'override di un finalizzatore.
                // TODO: impostare campi di grandi dimensioni su Null.

                disposedValue = true;
            }
        }

        // TODO: eseguire l'override di un finalizzatore solo se Dispose(bool disposing) include il codice per liberare risorse non gestite.
        // ~ConnectionHandler()
        // {
        //   // Non modificare questo codice. Inserire il codice di pulizia in Dispose(bool disposing) sopra.
        //   Dispose(false);
        // }

        // Questo codice viene aggiunto per implementare in modo corretto il criterio Disposable.
        public void Dispose()
        {
            // Non modificare questo codice. Inserire il codice di pulizia in Dispose(bool disposing) sopra.
            Dispose(true);
            // TODO: rimuovere il commento dalla riga seguente se è stato eseguito l'override del finalizzatore.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
