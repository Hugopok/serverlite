﻿using System;
using System.Collections.Generic;

namespace ServerLite.SL.Core
{
    [Serializable]
    public class ServerOptions
    {
        // Settings
        public int serverPort;
        public string serverIP;
        public int maxUsersCapacity;
        public bool clientNoDelay = true;
        public MongoDB.MongoCredential mongoCredential;

        public const int BUFFER_SIZE = 64000;

        // Ip banned Why not ??
        public List<string> ipsBanned = new List<string>();
    }
}
