﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Threading.Tasks;
using ServerLite.SL.Utils;
using ServerLite.SL.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using ServerLite.SL.LogSystem;

namespace ServerLite.SL.Core
{
    public abstract class Server
    {
        public static Action OnShutdown;

        public static ServerOptions Options;

        public string LocalIP { get { return this.localIP; } }
        public string PublicIP { get { return this.publicIP; } }
        public int Port { get { return this.port; } }

        public Action<MClient> OnClientConnected;

        protected TcpListener serverSocket;

        protected string localIP;
        protected string publicIP;
        protected int port;

        protected bool stopServer;

        protected List<ConnectionHandler> connections = new List<ConnectionHandler>();

        public Server()
        {
            Options = ReadConfigFile();

            port = Options.serverPort;

            AppDomain.CurrentDomain.ProcessExit += ApplicationClosing;

            this.Initialize(port);
        }

        public virtual async void StartServer()
        {
            while (!this.stopServer)
            {
                // Start an asynchronous socket to listen for connections.  
                Log.AddLog("Waiting for a connection...");

                // Wait until a client is not connected
                var client = await this.serverSocket.AcceptTcpClientAsync();

                if (client != null && client.Client != null)
                {
                    IPEndPoint clientEndPoint = (IPEndPoint)client.Client.RemoteEndPoint;

                    Log.AddLog($"Ip connected:{clientEndPoint.ToString()}");

                    if (Options.ipsBanned.Contains(clientEndPoint.ToString()))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Log.AddLog($"the following ip is banned {clientEndPoint.ToString()} i will kick off him");
                        Console.ForegroundColor = ConsoleColor.White;

                        client.Close();
                        client.Dispose();

                        continue;
                    }

                    ClientConnected(new MClient() { socket = client, endPoint = clientEndPoint.ToString(), id = Guid.NewGuid() });
                }
            }
        }

        protected abstract void ClientConnected(MClient mClient);

        protected virtual void Initialize(int port)
        {
            this.port = port;

            this.serverSocket = new TcpListener(IPAddress.Any, this.port);

            this.serverSocket.Server.NoDelay = Options.clientNoDelay;

            this.serverSocket.Start(Options.maxUsersCapacity);// max 5000 connection queue

            this.localIP = this.serverSocket.Server.LocalEndPoint.ToString();

            try
            {
                this.publicIP = IPHelper.GetPublicIP();
            }
            catch (Exception e)
            {
                Log.AddLog($"Error During retrieve the public ip: {e.Message}",true);
            }

            Console.WriteLine($"Server is running on public ip: {PublicIP}");

            Console.WriteLine($"Server is running on local ip: {LocalIP}");

            Options.serverIP = PublicIP;

            Task.Run(() => StartServer());
        }

        protected virtual void Shutdown()
        {
            OnShutdown?.Invoke();
        }

        protected virtual void ApplicationClosing(object sender, EventArgs e)
        {
            Log.AddLog("Application is closing");

            WriteConfigFile();
        }

        protected virtual ServerOptions ReadConfigFile()
        {
            string path = Path.Combine(Environment.CurrentDirectory, "Resources", "ServerOptions.json");

            return JsonConvert.DeserializeObject<ServerOptions>(File.ReadAllText(path));
        }

        protected static bool WriteConfigFile()
        {
            try
            {
                string path = Path.Combine(Environment.CurrentDirectory, "Resources", "ServerOptions.json");

                File.WriteAllText(path, JsonConvert.SerializeObject(Options, Formatting.Indented));

                return true;
            }
            catch (Exception e)
            {
                Log.AddLog($"Error during config file : {e.Message}");
            }

            return false;

        }
    }
}
