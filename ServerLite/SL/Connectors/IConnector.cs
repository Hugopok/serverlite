﻿using System;
using ServerLite.SL.Managers;
using System.Collections.Generic;

namespace ServerLite.SL.Connectors
{
    public interface IConnector
    {
        bool IsConnected { get; protected set; }
        List<IController> ControllersAttached { get; }
        void Send(IAsyncResult result);
        void Read(IAsyncResult result);
    }
}
