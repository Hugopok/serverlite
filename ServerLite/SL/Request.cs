﻿using System;
using System.Collections.Generic;
using System.Text;
using ServerLite.SL.Managers;

namespace ServerLite.SL
{
    [AttributeUsage(AttributeTargets.Method)]
    public class Request : Attribute
    {
        public string RequestType { get; private set; }

        public Request(string requestType)
        {
            RequestType = requestType;
        }
        
    }
}
