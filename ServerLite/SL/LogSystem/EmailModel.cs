﻿namespace ServerLite.SL.LogSystem
{
    public class EmailModel
    {
        public string Sender;
        public string To;
        public string Subject;
        public string Body;
        public bool IsBodyHtml;
        public string SmtpAddress;
        public int Port;
        public string Password;
    }
}
