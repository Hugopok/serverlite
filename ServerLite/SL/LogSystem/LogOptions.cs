﻿using System.Collections.Generic;

namespace ServerLite.SL.LogSystem
{
    public class LogOptions
    {
        public List<string> EmailsList = new List<string>();

        public string EmailSender;

        public string smtpHost;

        public int port;

        public string subject;

        public string smtpPassword;
    }
}
