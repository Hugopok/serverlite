﻿using System;
using System.IO;
using System.Text;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;

namespace ServerLite.SL.LogSystem
{
    [Serializable]
    public static class Log
    {
        private static StringBuilder _sBuilder = new StringBuilder();

        private static LogOptions _logOptions;

        private const string END_LOG_LINE = "---------------------------------------------------------------------------------";

        public static void SendLogToEmail(string txtPath)
        {
            string logJsonPath = Path.Combine(Environment.CurrentDirectory, "Resources", "LogOptions.json");

            LogOptions logOptions = JsonConvert.DeserializeObject<LogOptions>(File.ReadAllText(logJsonPath));

            _logOptions = logOptions;

            foreach (var email in _logOptions.EmailsList)
            {
                if (string.IsNullOrEmpty(email)) continue;

                Attachment fileToAttach = new Attachment(txtPath);

                SendMail(new EmailModel() 
                {
                    Subject = _logOptions.subject,
                    Password = _logOptions.smtpPassword,
                    Port = _logOptions.port,
                    Sender = _logOptions.EmailSender,
                    To = email,
                    SmtpAddress = _logOptions.smtpHost
                }, fileToAttach);

            }
        }

        public static void AddLog(string text,bool error = false)
        {
            string err = error ? "Error" : "Info";

            string logText = $"{DateTime.Now.ToString("F")} \n {err} || {text} \n \n \n {END_LOG_LINE}";

            _sBuilder.Append(logText);
        }

        public static void ClearStringBuilder()
        {
            _sBuilder.Clear();
        }

        public static void CreateFileLog(string path = "")
        {
            try
            {
                if (string.IsNullOrEmpty(path))
                    path = Path.Combine(Environment.CurrentDirectory, "Resources", "ServerLog.txt");

                if (!File.Exists(path))
                    File.Create(path);

                File.WriteAllText(path, _sBuilder.ToString());

                SendLogToEmail(path);
            }
            catch (IOException e)
            {
                Log.AddLog($"Error during file log creation (IOException) : {e.Message}",true);
            }
            catch (Exception e)
            {
                Log.AddLog($"Error during file log creation (GenericException) : {e.Message}",true);
            }
        }

        private static string SendMail(EmailModel email,Attachment fileToAttach)
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(email.Sender);
                    mail.To.Add(email.To);
                    mail.Subject = email.Subject;
                    mail.Body = email.Body;
                    mail.IsBodyHtml = email.IsBodyHtml;
                    //Way to add attachment
                    mail.Attachments.Add(fileToAttach);
                    using (SmtpClient smtp = new SmtpClient(email.SmtpAddress,email.Port))
                    {
                        smtp.Credentials = new NetworkCredential(email.Sender, email.Password);
                        smtp.EnableSsl = true;
                        smtp.Send(mail);
                        fileToAttach.ContentStream.Close();
                        fileToAttach.ContentStream.Dispose();
                        fileToAttach.Dispose();
                    }
                }
            }
            catch (SmtpFailedRecipientException exception)
            {
                AddLog("SMTP error in sending email", true);
                return "Error";
            }
            catch (Exception ex)
            {
                AddLog("Error in Sending Email", true);
                return "Error";
            }
            return string.Empty;
        }
    }
}

